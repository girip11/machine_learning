# Machine learning notes

This repository contains the following

* Notes, code snippets and materials from the [machine learning](https://www.udemy.com/share/101WciCUMfd1lVR34=/) course on udemy
* Guide on various ML packages like pandas, numpy and cheatsheets on tools like jupyter notebook.
* Notes from ML books that are part of my syllabus.

## Prerequisites

* Python programming

## Python packages used

* Numpy
* Pandas
* Scikit learn
* Jupyter notebook(optional if you can program using IDE like VSCode or Pycharm)
* Matplotlib

## Machine learning syllabus

### Mathematics

* [Linear algebra by Krista King](https://www.udemy.com/share/101v8oCUMfd1lVR34=/) in udemy.
* Probability course by Krista King in udemy.
* [Part I: Applied Math](https://www.deeplearningbook.org/)

### Machine learning algorithms

* [Udemy Machine learning course](https://www.udemy.com/share/101WciCUMfd1lVR34=/)
* [Andrew Ng course on machine learning](https://youtube.com/playlist?list=PLLssT5z_DsK-h9vYZkQkYNWcItqhlRJLN)
* [Hands–On Machine Learning with Scikit–Learn and TensorFlow](https://www.amazon.in/Hands-Machine-Learning-Scikit-Learn-TensorFlow/dp/1491962291)
* [Data Preparation for Machine Learning](https://machinelearningmastery.com/data-preparation-for-machine-learning/)

### Deep learning

* [Deep Learning for Coders with Fastai and PyTorch](https://www.amazon.in/Deep-Learning-Coders-fastai-PyTorch/dp/1492045527)
* [Deeplearningbook.org](https://www.deeplearningbook.org/)
* [Dive into Deep learning](https://d2l.ai/d2l-en.pdf)
* [Udemy Machine learning course](https://www.udemy.com/share/101WyWCUMfd1lVR34=/)

## Additional references

* Data Preparation for Machine Learning by Jason Brownlee

* [Machinelearningplus.com](https://www.machinelearningplus.com/) contains a lot of useful articles.

* [ML cheatsheet](https://ml-cheatsheet.readthedocs.io/en/latest)
