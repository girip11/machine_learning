# Algorithms to apply on common machine learning problems

## Problem Statement 1

To Predict the Housing Prices

Machine Learning Algorithm(s) to solve the problem —

    Advanced regression techniques like random forest and gradient boosting

## Problem Statement 2

Explore customer demographic data to identify patterns

Machine Learning Algorithm(s) to solve the problem —

    Clustering (elbow method)

## Problem Statement 3

Predicitng Loan Repayment

Machine Learning Algorithm(s) to solve the problem —

    Classification Algorithms for imbalanced dataset

## Problem Statement 4

Predict if a skin lesion is benign or malignant based on its characteristics (size, shape, color, etc)

Machine Learning Algorithm(s) to solve the problem —

    Convolutional Neural Network ( U-Net being the best for segmentation stuffs)

## Problem Statement 5

Predict client churn

Machine Learning Algorithm(s) to solve the problem —

    Linear discriminant analysis (LDA) or Quadratic discriminant analysis (QDA)

( particularly popular because it is both a classifier and a dimensionality reduction technique)

## Problem Statement 6

Provide a decision framework for hiring new employees

Machine Learning Algorithm(s) to solve the problem —

    Decision Tree is a pro gamer here

## Problem Statement 7

Understand and predict product attributes that make a product most likely to be purchased

Machine Learning Algorithm(s) to solve the problem —

    Logistic Regression
    Decision Tree

## Problem Statement 8

Analyze sentiment to assess product perception in the market.

Machine Learning Algorithm(s) to solve the problem —

    Naive Bayes — Support Vector Machines (NBSVM)

## Problem Statement 9

Create classification system to filter out spam emails

Machine Learning Algorithm(s) to solve the problem —

    Classification Algorithms —

Naive Bayes, SVM , Multilayer Perceptron Neural Networks (MLPNNs) and Radial Base Function Neural Networks (RBFNN) suggested.

## Problem Statement 10

Predict how likely someone is to click on an online ad

Machine Learning Algorithm(s) to solve the problem —

    Logistic Regression
    Support Vector Machines

## Problem Statement 11

Detect fraudulent activity in credit-card transactions.

Machine Learning Algorithm(s) to solve the problem —

    Adaboost
    Isolation Forest
    Random Forest

## Problem Statement 12

Predict the price of cars based on their characteristics

Machine Learning Algorithm(s) to solve the problem —

    Gradient-boosting trees are best at this.

## Problem Statement 13

Predict the probability that a patient joins a healthcare program

Machine Learning Algorithm(s) to solve the problem —

    Simple neural networks

## Problem Statement 14

Predict whether registered users will be willing or not to pay a particular price for a product.

Machine Learning Algorithm(s) to solve the problem —

    Neural Networks

## Problem Statement 15

Segment customers into groups by distinct charateristics (eg, age group)

Machine Learning Algorithm(s) to solve the problem —

    K-means clustering

## Problem Statement 16

Feature extraction from speech data for use in speech recognition systems

Machine Learning Algorithm(s) to solve the problem —

    Gaussian mixture model

## Problem Statement 17

Object tracking of multiple objects, where the number of mixture components and their means predict object locations at each frame in a video sequence.

Machine Learning Algorithm(s) to solve the problem —

    Gaussian mixture model

## Problem Statement 18

Organizing the genes and samples from a set of microarray experiments so as to reveal biologically interesting patterns.

Machine Learning Algorithm(s) to solve the problem —

    Hierarchical clustering algorithms

## Problem Statement 19

Recommend what movies consumers should view based on preferences of other customers with similar attributes.

Machine Learning Algorithm(s) to solve the problem —

    Recommender system

## Problem Statement 20

Recommend news articles a reader might want to read based on the article she or he is reading.

Machine Learning Algorithm(s) to solve the problem —

    Recommender system

## Problem Statement 21

Recommend news articles a reader might want to read based on the article she or he is reading.

Machine Learning Algorithm(s) to solve the problem —

    Recommender system

## Problem Statement 22

Optimize the driving behavior of self-driving cars

Machine Learning Algorithm(s) to solve the problem —

    Reinforcement Learning

## Problem Statement 23

Diagnose health diseases from medical scans.

Machine Learning Algorithm(s) to solve the problem —

    Convolutional Neural Networks

## Problem Statement 24

Balance the load of electricity grids in varying demand cycles

Machine Learning Algorithm(s) to solve the problem —

    Reinforcement Learning

## Problem Statement 25

When you are working with time-series data or sequences (eg, audio recordings or text)

Machine Learning Algorithm(s) to solve the problem —

    Recurrent neural network
    LSTM

## Problem Statement 26

Provide language translation

Machine Learning Algorithm(s) to solve the problem —

    Recurrent neural network

## Problem Statement 27

Generate captions for images

Machine Learning Algorithm(s) to solve the problem —

    Recurrent neural network

## Problem Statement 28

Power chatbots that can address more nuanced customer needs and inquiries

Machine Learning Algorithm(s) to solve the problem —

    Recurrent neural network

---

## References

* [Which Machine Learning Algorithm Should You Use By Problem Type?](https://medium.com/analytics-vidhya/which-machine-learning-algorithm-should-you-use-by-problem-type-a53967326566)
