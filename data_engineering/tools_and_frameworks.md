# Tools and Frameworks for data analytics and engineering

Here I will capture all the tools and frame works for data engineering, analytics.

- Mojo programming language from Modular. Can utilize the hardware better and is much faster compared to python.
- Modin project makes the pandas code execute in a distributed fashion. Supports multiple backends like ray, dask, unidist etc. Works on single machine as well as distributed setup
- Apache arrow - inmemory data format that is language agnostic. Parquet is the format in which the data is stored on the disk and is compressed.
- Duckdb - embeddable database like sqlite but for analytical workloads.
- Polars high speed dataframe processing library in Rust and python.
- Malloy is advanced query language that will transpile to SQL.
- Apache Iceberg - Open table format for huge analytics workload.
- Apache Calcite - modular query optimization
- [Lance](https://github.com/lancedb/lance) - Modern columnar data format for ML and LLMs implemented in Rust.
