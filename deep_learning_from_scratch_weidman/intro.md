# Deep learning from scratch

## Neural network mental models

A neural network is

- a mathematical function that takes inputs and produces output.
- a universal function approximator that can in theory represent the solution to any supervised learning problem
- a computational graph through which multidimensional arrays flow.
- is made up of layers each of which can be thought of as having a number of neurons.
