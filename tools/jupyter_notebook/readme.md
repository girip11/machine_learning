# Using jupyter notebooks

---

## References

* [How to use Jupyter Notebooks in 2020 (Part 1: The data science landscape)](https://ljvmiranda921.github.io/notebook/2020/03/06/jupyter-notebooks-in-2020/)
* [How to use Jupyter Notebooks in 2020 (Part 2: Ecosystem growth)](https://ljvmiranda921.github.io/notebook/2020/03/16/jupyter-notebooks-in-2020-part-2/)
* [How to use Jupyter Notebooks in 2020 (Part 3: Final thoughts)](https://ljvmiranda921.github.io/notebook/2020/03/30/jupyter-notebooks-in-2020-part-3/)
