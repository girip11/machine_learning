# Jupyter Notebook Shortcuts

Jupyter notebook has two modes

* Command mode
* Edit mode

## Shortcuts in both modes

* `Shift + Enter` - run current cell and select below cell
* `Ctrl + Enter` - run selected cells
* `Ctrl + S` - Save the notebook

## Command mode shortcuts

`Esc` to activate command mode.

* `Enter` - take you into edit mode
* `H` - Help to show all shortcuts
* `Up` - select cell above and `Down` select cell below
* `Shift + Up` - extend selected cells above
* `Shift + Down` - extend selected cells below
* `A` - insert cell above
* `B` - insert cell below
* `X` - cut selected cells
* `C` - copy selected cells
* `V` - paste cells below
* `Shift + V` - paste cells above
* `Press D twice` - Deletes the current cell
* `Z` - undo cell deletion
* `S` -  Save and Checkpoint
* `Y` - change the cell type to Code
* `M` - change the cell type to Markdown
* `P` - open the command palette.

## Edit Mode shortcuts

Press `Enter` in command mode to enter the edit mode.

* `ESC` - switches to command mode.
* `Tab` - code completion or indent
* `Shift + Tab` - tooltip
* `Ctrl + ]`  -indent
* `Ctrl + [` - dedent
* `Ctrl + A`  -select all
* `Ctrl + Z`  -undo
* `Ctrl + Shift + Z` - redo
* `Ctrl + Home` - go to cell start
* `Ctrl + End` - go to cell end
* `Ctrl + Left` - go one word left
* `Ctrl + Right` - go one word right
* `Ctrl + Shift + P` - open the command palette
* `Down` - move cursor down
* `Up`  - move cursor up

A list of interesting jupyter notebooks collection can be found [here](https://github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks)

---

## References

* [Jupyter Notebook Shortcuts by Ventsislav Yordanov](https://towardsdatascience.com/jypyter-notebook-shortcuts-bf0101a98330)
* [Jupyter notebook basics](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)
* [Jupyter notebook advanced tutorial](https://www.dataquest.io/blog/advanced-jupyter-notebooks-tutorial/)
* [Jupyter notebook tricks and tips](https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/)
