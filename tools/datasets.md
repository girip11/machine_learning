# Machine learning datasets

* [Kaggle datasets](https://www.kaggle.com/datasets)
* [UCI machine learning datasets repository](http://archive.ics.uci.edu/ml/index.)php
* [Jason Brownlee datasests](https://github.com/jbrownlee/Datasets)
* [Awesome public datasets repo](https://github.com/awesomedata/awesome-public-datasets)
