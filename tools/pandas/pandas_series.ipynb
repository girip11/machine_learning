{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Tools - pandas**\n",
    "\n",
    "*The `pandas` library provides high-performance, easy-to-use data structures and data analysis tools. The main data structure is the `DataFrame`, which you can think of as an in-memory 2D table (like a spreadsheet, with column names and row labels). Many features available in Excel are available programmatically, such as creating pivot tables, computing columns based on other columns, plotting graphs, etc. You can also group rows by column value, or join tables much like in SQL. Pandas is also great at handling time series.*\n",
    "\n",
    "Prerequisites:\n",
    "* NumPy – if you are not familiar with NumPy, we recommend that you go through the [NumPy tutorial](tools_numpy.ipynb) now."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's import `pandas`. People usually import it as `pd`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `Series` objects\n",
    "The `pandas` library contains these useful data structures:\n",
    "* `Series` objects, that we will discuss now. A `Series` object is 1D array, similar to a column in a spreadsheet (with a column name and row labels).\n",
    "* `DataFrame` objects. This is a 2D table, similar to a spreadsheet (with column names and row labels).\n",
    "* `Panel` objects. You can see a `Panel` as a dictionary of `DataFrame`s. These are less used, so we will not discuss them here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a `Series`\n",
    "Let's start by creating our first `Series` object!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    2\n",
       "1   -1\n",
       "2    3\n",
       "3    5\n",
       "dtype: int64"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = pd.Series([2, -1, 3, 5])\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Similar to a 1D `ndarray`\n",
    "`Series` objects behave much like one-dimensional NumPy `ndarray`s, and you can often pass them as parameters to NumPy functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0      7.389056\n",
       "1      0.367879\n",
       "2     20.085537\n",
       "3    148.413159\n",
       "dtype: float64"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "np.exp(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arithmetic operations on `Series` are also possible, and they apply *elementwise*, just like for `ndarray`s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    1002\n",
       "1    1999\n",
       "2    3003\n",
       "3    4005\n",
       "dtype: int64"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s + [1000, 2000, 3000, 4000]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to NumPy, if you add a single number to a `Series`, that number is added to all items in the `Series`. This is called *broadcasting*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    1002\n",
       "1     999\n",
       "2    1003\n",
       "3    1005\n",
       "dtype: int64"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s + 1000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same is true for all binary operations such as `*` or `/`, and even conditional operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    False\n",
       "1     True\n",
       "2    False\n",
       "3    False\n",
       "dtype: bool"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s < 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Index labels\n",
    "Each item in a `Series` object has a unique identifier called the *index label*. By default, it is simply the rank of the item in the `Series` (starting at `0`) but you can also set the index labels manually:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "alice       68\n",
       "bob         83\n",
       "charles    112\n",
       "darwin      68\n",
       "dtype: int64"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s2 = pd.Series([68, 83, 112, 68], index=[\"alice\", \"bob\", \"charles\", \"darwin\"])\n",
    "s2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can then use the `Series` just like a `dict`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "83"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s2[\"bob\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can still access the items by integer location, like in a regular array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "83"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s2[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To make it clear when you are accessing by label or by integer location, it is recommended to always use the `loc` attribute when accessing by label, and the `iloc` attribute when accessing by integer location:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "83"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# use with label and boolean mask\n",
    "s2.loc[\"bob\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "83"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s2.iloc[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Slicing a `Series` also slices the index labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "bob         83\n",
       "charles    112\n",
       "dtype: int64"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s2.iloc[1:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can lead to unexpected results when using the default numeric labels, so be careful:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    1000\n",
       "1    1001\n",
       "2    1002\n",
       "3    1003\n",
       "dtype: int64"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "surprise = pd.Series([1000, 1001, 1002, 1003])\n",
    "surprise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2    1002\n",
       "3    1003\n",
       "dtype: int64"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "surprise_slice = surprise[2:]\n",
    "surprise_slice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oh look! The first element has index label `2`. The element with index label `0` is absent from the slice:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Key error: 0\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    surprise_slice[0]\n",
    "except KeyError as e:\n",
    "    print(\"Key error:\", e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But remember that you can access elements by integer location using the `iloc` attribute. This illustrates another reason why it's always better to use `loc` and `iloc` to access `Series` objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1002"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "surprise_slice.iloc[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Init from `dict`\n",
    "You can create a `Series` object from a `dict`. The keys will be used as index labels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "alice     68\n",
       "bob       83\n",
       "colin     86\n",
       "darwin    68\n",
       "dtype: int64"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "weights = {\"alice\": 68, \"bob\": 83, \"colin\": 86, \"darwin\": 68}\n",
    "s3 = pd.Series(weights)\n",
    "s3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can control which elements you want to include in the `Series` and in what order by explicitly specifying the desired `index`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "colin    86\n",
       "alice    68\n",
       "dtype: int64"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s4 = pd.Series(weights, index=[\"colin\", \"alice\"])\n",
    "s4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Automatic alignment\n",
    "When an operation involves multiple `Series` objects, `pandas` automatically aligns items by matching index labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Index(['alice', 'bob', 'charles', 'darwin'], dtype='object')\n",
      "Index(['alice', 'bob', 'colin', 'darwin'], dtype='object')\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "alice      136.0\n",
       "bob        166.0\n",
       "charles      NaN\n",
       "colin        NaN\n",
       "darwin     136.0\n",
       "dtype: float64"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "print(s2.keys())\n",
    "print(s3.keys())\n",
    "\n",
    "s2 + s3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting `Series` contains the union of index labels from `s2` and `s3`. Since `\"colin\"` is missing from `s2` and `\"charles\"` is missing from `s3`, these items have a `NaN` result value. (ie. Not-a-Number means *missing*).\n",
    "\n",
    "Automatic alignment is very handy when working with data that may come from various sources with varying structure and missing items. But if you forget to set the right index labels, you can have surprising results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "s2 = [ 68  83 112  68]\n",
      "s5 = [1000 1000 1000 1000]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "alice     NaN\n",
       "bob       NaN\n",
       "charles   NaN\n",
       "darwin    NaN\n",
       "0         NaN\n",
       "1         NaN\n",
       "2         NaN\n",
       "3         NaN\n",
       "dtype: float64"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s5 = pd.Series([1000, 1000, 1000, 1000])\n",
    "print(\"s2 =\", s2.values)\n",
    "print(\"s5 =\", s5.values)\n",
    "\n",
    "s2 + s5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas could not align the `Series`, since their labels do not match at all, hence the full `NaN` result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Init with a scalar\n",
    "You can also initialize a `Series` object using a scalar and a list of index labels: all items will be set to the scalar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "life          42\n",
       "universe      42\n",
       "everything    42\n",
       "dtype: int64"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "meaning = pd.Series(42, [\"life\", \"universe\", \"everything\"])\n",
    "meaning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `Series` name\n",
    "A `Series` can have a `name`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "bob      83\n",
       "alice    68\n",
       "Name: weights, dtype: int64"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s6 = pd.Series([83, 68], index=[\"bob\", \"alice\"], name=\"weights\")\n",
    "s6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting a `Series`\n",
    "Pandas makes it easy to plot `Series` data using matplotlib (for more details on matplotlib, check out the [matplotlib tutorial](tools_matplotlib.ipynb)). Just `import matplotlib` and call the `plot()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXQAAAD4CAYAAAD8Zh1EAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjQuMywgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/MnkTPAAAACXBIWXMAAAsTAAALEwEAmpwYAAAmg0lEQVR4nO3dd3jV5f3/8ec7mwwIJGGHGYbsERmCVNwDxbrrrvqlVlyt46t2t7baatW6f0rdmziwVnHiwIEm7E3YYSaMkEH2/fuDo19KGSE5yZ1z8npcV66c8THndYTrlQ/3uT/3bc45REQk9EX4DiAiIsGhQhcRCRMqdBGRMKFCFxEJEyp0EZEwEeXrhVNTU123bt18vbyISEjKyckpcM6l7e85b4XerVs3srOzfb28iEhIMrO1B3pOQy4iImFChS4iEiZU6CIiYUKFLiISJlToIiJhQoUuIhImVOgiImHC2zx08aukvIpvV29n+ZYiBnVOZljXZGKjIn3HEpF6UKE3E5XVNcxbv5OZuQV8lbuN2et2UFXzf2vhx0VHMKJ7CmMzUjiqZyr9OrQkIsI8JhaRw6VCD1POOZZvKWZmbgFf5hYwa9U2SiqqMYOBnVrxP+N6MDYjlT7tk5i7bucPx/3l3aUAtI6P5qiMVMZmpDKmZypdUuI9vyMRORQVehjZsHM3XwaK+cvcbRQUlwPQPTWBHw/rxNiMVEb1SCE5PuY//rvj+7Xj+H7tANiyq4yvVhYwc8U2vswt4N/zNwGQ3qYFYzNSOapnKkf1TCElMbZx35yIHJL52oIuMzPTaS2X+iksreTrVQWBs+ttrC4oASA1MYYxGak/fHVKblGnn++cY2V+SaDgC/h61TaKyqoA6NehJWN77Sn3Ed3bEB+jcwORxmBmOc65zP0+p0IPHWWV1eSs3fHD8MiCDYU4BwkxkYzskcKYwBBJ73aJmAV//LuquoYFGwr5auU2Zq4oIGftDiqqa4iONIZ1af3DL5DBnVsRFakJVCINQYUeoqprHIs2Fv5Q4NlrdlBeVUNUhDG0S/IPBT44PZloDwW6u6Ka7LXbf8i3aOMunIOk2KjAL5gUxmakktG2YX7BiDRHByt0/Tu5idlUuJuPlmzly8AQR+HuSgD6tk/i4lFdGZuRypHd25AY6/+PrkVMJEf3SuPoXnuWZt5RUsHXq7b9UPAfLdkCQNuk2D3j7xmp9G2fREQjlXtkhNGrbaJm60izoTP0JqRwdyVH//UTdpVV0Sm5BWMy9gyjHNUzlbSk0PsQcv320j3j77nb+Cq3gG0lFY2e4Zg+aTxxSSYxURoCkvCgM/QQ8c78jewqq+KFK0cyJiMl5Icp0tvEc36bLpx/ZBdqahxLNxexbntpo73+ii1F/P3D5dz46hwevGCoxvUl7KnQm5Cp2Xn0aZcUFmW+r4gIo1/HlvTr2LLRXvPkAe1pERPJnf9eQnzMAv529iANv0hYU6E3Eblbi5i7fie/OvWIsCtzn646ugfF5VU88NEKEmOj+N3p/fT/V8KWCr2JmJqTR2SEcebQTr6jhJ0bjutFUVkV/5y5mqS4KG46sY/vSCINolaDimaWbGZZZrbUzJaY2eh9njcze9DMcs1svpkNa5i44amquoY3Zm9gfJ+2IfnhZ1NnZvz6tCO44Mh0Hvokl8c/W+k7kkiDqO0Z+j+A6c65c8wsBth3YY9TgF6Br5HAY4HvUgufr8gnv6icczM7+44StsyMP/94IMXlVdz93lISY6O4eFRX37FEguqQhW5mrYBxwOUAzrkKYN/5ZxOB59yeOZDfBM7oOzjnNgU5b1jKysmjTUIM4/u09R0lrEVGGPefP4TdFdX8ZtpCEmOjNMQlYaU2Qy7dgXzgaTObY2ZTzCxhn2M6Aev3up8XeOw/mNkkM8s2s+z8/Pw6hw4nO0oq+GjxVs4c0klzpRtBdGQEj1w0jFHdU7hp6jw+WLTZdySRoKlNg0QBw4DHnHNDgRLgtrq8mHPuCedcpnMuMy0trS4/IuxMm7uBiuoaDbc0orjoSJ68LJOBnVpx7UtzmLmiwHckkaCoTaHnAXnOuVmB+1nsKfi9bQDS97rfOfCYHMLUnDwGdGrJER0ab362QGJsFM/89Eh6pCXwP89lk7N2u+9IIvV2yEJ3zm0G1pvZ93O9jgMW73PY28Clgdkuo4BCjZ8f2uKNu1i0cRfnDNPZuQ/J8TE8f+VI2reK4/Knv2PhhkLfkUTqpbaDttcBL5rZfGAI8Bczu9rMrg48/y6wCsgFngSuCXbQcJSVk0dMZAQTh+iDOV/SkmJ54aqRJMVGcelT35K7tch3JJE60+JcnlRU1TDqro8Z1aMNj1403HecZm91QQnnPv41URHG1KtHk95GW+5J03Swxbk0rcKTT5ZuZXtJBecOTz/0wdLguqcm8PyVI9hdWc1FU2axZVeZ70gih02F7klWTh5tk2I5uleq7ygScESHljzz0yPZVlzOxVNmsd3Dcr8i9aFC9yC/qJwZy7by42GdtKRrEzO0S2umXHYk67aXctlT37KrrNJ3JJFaU5t48NacDVTXOA23NFGje6bw2MXDWLJpF1c9k83uimrfkURqRYXeyJxzTM1Zz9AuyWS0TfQdRw7g2L7teOCCIWSv3c7PXsihvEqlLk2fCr2Rzc8rZPmWYs4ZrrnnTd2EQR25+6xBfL48nxtfmUtVdY3vSCIHpUJvZFk5ecRGRXD64I6+o0gtnHdkOr+Z0I/3Fm7mf19fQE2Nn2m+IrWhDS4aUVllNdPmbuDkAe1pGRftO47U0pVju1NcVsX9Hy0nMTaS35/RX7seSZOkQm9EHy7ewq6yKn0YGoKuPy6D4vJKnvxiNYlxUdxyUl/fkUT+iwq9EU3NyaNjqzhG90zxHUUOk5lxx6lHUFxexSMzVpIQG8U1x2T4jiXyH1TojWRzYRkzV+QzeXwGkdp5PiSZGXeeOZCS8mr+Nn0ZSbFRXDK6m+9YIj9QoTeS12fnUePQ7JYQFxlh/P28wZRWVPGbaYuIj4nibP2ZShOhWS6NwDlHVk4eI7q3oWvKvps9SaiJjozg4QuHcVTPFG7Jmsf0hdr1SJoGFXojyFm7g9UFJTo7DyNx0ZE8eWkmg9OTuf7lOXy+XFsqin8q9EaQlZNHfEwkpw3s4DuKBFFCbBTPXD6Cnm0TmfR8Nt+t0a5H4pcKvYGVVlTxzvxNnDqwAwmx+sgi3LSKj+a5K0bQsVULrnj6O3K3FvuOJM2YCr2BTV+4meLyKs7VcEvYSkuK5fmrRhITFcHkF2drMS/xRoXewKZm59GlTTwjurfxHUUaUKfkFtx//hCWby3it9MW+o4jzZQKvQGt317K16u2cc7wzrpUvBkY1zuN68ZnMDUnj6nZ633HkWZIhd6AXp+dhxmap9yM3HB8b0b3SOE30xaybLM2nJbGpUJvIDU1e+aej+mZSqfkFr7jSCOJjDD+8ZMhJMZG8/MXcygpr/IdSZoRFXoD+Wb1NvJ27Nbc82aobVIcD/5kCGsKSrjjzQU4pyV3pXGo0BtIVk4eSbFRnNS/ve8o4sFRPVP5xfG9mTZ3Iy9/q/F0aRwq9AZQXF7Fews2M2FwR1rERPqOI55MHp/BuN5p/P5fi1i4odB3HGkGVOgN4N/zN7K7sppzMzXc0pxFRBj3nzeYNvExXPvSbHaVVfqOJGFOhd4Apmbn0SMtgaHpyb6jiGcpibE8dOFQ1u/YzW2vz9d4ujQoFXqQrS4oIXvtDs4dnq655wLAkd3acMtJfXh3wWae+3qt7zgSxmpV6Ga2xswWmNlcM8vez/PHmFlh4Pm5Zvbb4EcNDVk564kwOGtYJ99RpAmZdHQPjuvbljv/vZh563f6jiNh6nDO0Mc754Y45zIP8PwXgeeHOOf+GIxwoaa6xvF6zgZ+1DuNdi3jfMeRJiQisDFG26Q4Jr80m8JSjadL8GnIJYhm5haweVcZ52gTaNmP5PgYHr5wKFt2lXFz1jyNp0vQ1bbQHfCBmeWY2aQDHDPazOaZ2Xtm1n9/B5jZJDPLNrPs/Pzw2xAgKyeP5Phoju/X1ncUaaKGdmnNbaccwYeLt/DPmat9x5EwU9tCH+ucGwacAkw2s3H7PD8b6OqcGww8BLy1vx/inHvCOZfpnMtMS0ura+YmqbC0kvcXbWbi4I7ERmnuuRzYFWO6cVL/dtz93lJy1u7wHUfCSK0K3Tm3IfB9K/AmMGKf53c554oDt98Fos0sNchZm7S352+koqpGwy1ySGbG384ZTIfkOK59aTbbSyp8R5IwcchCN7MEM0v6/jZwIrBwn2PaW2COnpmNCPzcbcGP23RlZa+nb/skBnRq6TuKhIBWLaJ59MLhbCuu4JevzaWmRuPpUn+1OUNvB8w0s3nAt8C/nXPTzexqM7s6cMw5wMLAMQ8CF7hm9InP8i1FzMsr1LrnclgGdm7FbyYcwafL8nn885W+40gYOOQml865VcDg/Tz++F63HwYeDm600JGVk0dUhHHmUM09l8Nz8aiuzFq9nXvfX8bwLq0Z2SPFdyQJYZq2WE+V1TW8MXsD4/u2JTUx1nccCTFmxl1nDaRrSgLXvTyHguJy35EkhKnQ6+mzZfkUFJdrE2ips6S4aB65cBiFuyu58ZW5VGs8XepIhV5PWTl5pCbGML6v5p5L3fXr2JI/nNGfmbkFPPxJru84EqJU6PWwvaSCj5du4cwhnYiO1P9KqZ/zj0znrKGdeODj5XyZW+A7joQgtVA9vDVnA5XVjnO07rkEgZlx548H0DMtkRtemcPWXWW+I0mIUaHXw9ScPAZ2akXf9pp7LsERHxPFYxcNo6S8mutenkNVdY3vSBJCVOh1tGhjIUs27dKuRBJ0vdolceeZA5i1ejv3f7TcdxwJISr0OpqanUdMZARnDO7oO4qEobOHd+b8zHQembGSGcu2+o4jIUKFXgcVVTVMm7uBE/q1Izk+xnccCVN/mNifvu2T+OWrc9m4c7fvOBICVOh18PGSLewordSHodKg4qIjefSiYVRU1XDdy3Oo1Hi6HIIKvQ6ycvJo1zKWcb3CawlgaXp6pCVy99mDyFm7g3veX+Y7jjRxKvTDtLWojE+X53PWsM5ERmghLml4pw/uyMWjuvDE56v4cPEW33GkCVOhH6Y3Z2+gusZxji71l0b069P6MaBTS256bS7rt5f6jiNNlAr9MDjnmJqTx7AuyfRMS/QdR5qRuOhIHrlwGM7BtS/NpqJK4+ny31Toh2FeXiG5W4s5N1O7Eknj65qSwD3nDmJeXiF/eXeJ7zjSBKnQD8PU7PXERUdw2qAOvqNIM3XygA78dEw3nvlqDe8t2OQ7jjQxKvRaKqus5u15Gzm5f3taxkX7jiPN2O2nHMGQ9GRuzZrPmoIS33GkCVGh19L7izZTVFal4RbxLiYqgocvHEpEhDH5pdmUVVb7jiRNhAq9lrJy8uiU3ILR2iJMmoDOreO577zBLNq4iz+9s9h3HGkiVOi1sHHnbmbmFnD28M5EaO65NBHHHdGOn43rwYuz1jFt7gbfcaQJUKHXwhuz83AOzhmmuefStNx8Uh8yu7bmjjcWsDK/2Hcc8UyFfgjOObJy8hjZvQ1dUuJ9xxH5D9GRETx04VBioyOZ/OJsdldoPL05U6EfwndrdrBmW6k+DJUmq0OrFtx33mCWbSnid28v9B1HPFKhH8LL364jISaSUwe29x1F5ICO6dOWycdk8Fp2Hlk5eb7jiCcq9INYtrmIaXM38JMRXYiPifIdR+Sgbjy+F6N6tOHXby1g+ZYi33HEAxX6Qfxt+lISYqOYPD7DdxSRQ4qKjODBC4aSGBvNNS/OpqS8ynckaWQq9AOYtWobHy/dyjXHZNA6QbsSSWho2zKOBy8Ywsr8Yn791kKcc74jSSNSoe+Hc467py+lfcs4fjqmm+84IoflqIxUbjyuN2/O2cCr3633HUcaUa0K3czWmNkCM5trZtn7ed7M7EEzyzWz+WY2LPhRG8/7izYzZ91OfnFCL+KiI33HETls1x6bwdG9Uvnt24tYvHGX7zjSSA7nDH28c26Icy5zP8+dAvQKfE0CHgtGOB+qqmv42/RlZLRN5GxdSCQhKjLCuP/8IbSOj2byS7MpKqv0HUkaQbCGXCYCz7k9vgGSzSwk15h9LTuPVQUl3HpSH6IiNSIloSs1MZYHLxjKuu2l3PbGAo2nNwO1bSwHfGBmOWY2aT/PdwL2HqzLCzz2H8xskpllm1l2fn7+4adtYKUVVTzw0XIyu7bmhH7tfMcRqbeRPVK46cTe/Hv+Jl74Zq3vONLAalvoY51zw9gztDLZzMbV5cWcc0845zKdc5lpaWl1+REN6ukv17C1qJzbTumLmRbhkvBw9biejO+Txp/eWcKCvELfcaQB1arQnXMbAt+3Am8CI/Y5ZAOw97XxnQOPhYztJRU8/ulKTujXjsxubXzHEQmaiAjjvvOGkJoYwzUv5VC4W+Pp4eqQhW5mCWaW9P1t4ERg3wUj3gYuDcx2GQUUOudCan+shz/JpaSiiltP6uM7ikjQtU6I4aELh7FpZxm3Zs3TeHqYqs0ZejtgppnNA74F/u2cm25mV5vZ1YFj3gVWAbnAk8A1DZK2gazfXsrz36zh3OHp9GqX5DuOSIMY3rU1t53Sl/cXbeGpL9f4jiMN4JALlDjnVgGD9/P443vddsDk4EZrPPd9uJwIM35xQm/fUUQa1JVjuzNr9XbuencJQ7skM6xLa9+RJIia/by8RRsLeWvuBq4Y2532reJ8xxFpUGbGvecMpn2rOK57aQ47Syt8R5IgavaF/tfpy2gZF83VP+rpO4pIo2gVH82jFw0jv6icm16bR02NxtPDRbMu9C9zC/h8eT7Xjs+gVYto33FEGs2gzsn86rQj+HjpVp74YpXvOBIkzbbQa2ocd7+3lE7JLbhkdFffcUQa3aWju3LawA7c8/4yvluz3XccCYJmW+jvLtzEgg2F/PKE3lqAS5olM+PusweS3roF1740m23F5b4jST01y0KvrK7hnveX0bd9EmcO/a8VCkSajaS4aB65aBg7Siu58dW5Gk8Pcc2y0F/+dh1rt5Xyvyf3JTJCl/hL89a/Yyt+f3p/vlhRwCMzcn3HkXpodoVeXF7Fgx+vYGT3NhzTp+mtJyPiw09GpHPmkI7c/9FyvlpZ4DuO1FGzK/QpX6yioLhCC3CJ7MXM+POPB9I9NYHrX57L1qIy35GkDppVoecXlfPk56s4dWB7huoKOZH/kBAbxaMXDae4vJIbXp5LtcbTQ06zKvSHP1lBWVUNN5+oBbhE9qdP+yT+NHEAX6/axj8+Wu47jhymZlPoawpKeHHWOi44Mp0eaYm+44g0WedmpnPu8M48NCOXz5c3vY1o5MCaTaHf+8EyoiMjuOG4Xr6jiDR5f5w4gN5tk7jx1blsLtR4eqhoFoU+P28n78zfxFVHd6dtSy3AJXIoLWIieeSiYZRVVnPdy7Opqq7xHUlqIewL3bk9l/i3SYhh0rgevuOIhIyMtoncddZAvluzg3s/0Hh6KAj7Qv98RQFfrdzGdcdmkBSnBbhEDsfEIZ24cGQXHv9sJTOWbfUdRw4hrAv9+wW40tu04MKRXXzHEQlJv53Qjz7tkrjjjQUUl1f5jiMHEdaF/va8jSzZtIubT+xDbJQW4BKpi7joSO46eyCbd5Xx9w+W+Y4jBxG2hV5eVc29Hyyjf8eWnD6oo+84IiFtWJfWXDKqK898tYZ563f6jiMHELaF/uI368jbsZvbTulLhBbgEqm3W07qQ9ukWG5/Y4FmvTRRYVnou8oqeeiTFYzNSOXoXlqASyQYkuKi+cMZ/Vm8aRdPfbnadxzZj7As9Cc+W8WO0kr+9+S+vqOIhJWT+rfn+CPacf+HK1i/vdR3HNlH2BX61l1lTJm5itMHd2Rg51a+44iEFTPjjxP7E2Hw67cW4pwW8GpKwq7QH/h4BdU1jptP7O07ikhY6pjcgptP6sNny/P51/xNvuPIXsKq0FfmF/Pqd+u5aGRXuqYk+I4jErYuHd2NwZ1b8cd/LaKwtNJ3HAkIq0K/Z/oy4qIiuPbYDN9RRMJaZITxl7MGsqO0krveW+I7jgSETaHPXreD6Ys2M2lcT1ITY33HEQl7/Tu24sqx3Xnlu/V8u3q77zhCmBS6c467311KamIMVx3d3XcckWbjxuN70bl1C25/Yz7lVdW+4zR7tS50M4s0szlm9s5+nrvczPLNbG7g66rgxjy4Gcu28u2a7dxwXC8SYqMa86VFmrX4mCjuPHMAK/NLePzTVb7jNHuHc4Z+A3CwwbJXnXNDAl9T6pmr1qprHH99bxndUuK5YIQW4BJpbMf0acvpgzvyyIxccrcW+47TrNWq0M2sM3Aa0GhFXVtvzM5j2ZYibjmpL9GRYTGCJBJyfjuhH3HREfzqzQWam+5RbRvwAeBW4GALOJxtZvPNLMvM0vd3gJlNMrNsM8vOz6//XoVlldXc9+FyBnduxakD29f754lI3aQlxXLHqUcwa/V2pmbn+Y7TbB2y0M1sArDVOZdzkMP+BXRzzg0CPgSe3d9BzrknnHOZzrnMtLT6r7Hy3Ndr2FRYxv+e0hczLcAl4tN5memM6NaGP7+7hILict9xmqXanKGPAc4wszXAK8CxZvbC3gc457Y5577/E5wCDA9qyv0oLK3kkRkr+VHvNI7qmdrQLycihxARYfzlrAGUVlTxp3cW+47TLB2y0J1ztzvnOjvnugEXAJ845y7e+xgz67DX3TM4+IenQfHoZ7nsKtMCXCJNSUbbJH5+TAbT5m7ks+X1H1aVw1PnTxHN7I9mdkbg7vVmtsjM5gHXA5cHI9yBbNy5m6e/XMOPh3SiX8eWDflSInKYrjmmJz3SEvj1WwvYXaG56Y3psArdOfepc25C4PZvnXNvB27f7pzr75wb7Jwb75xb2hBhv/fAR8vBwS9O0AJcIk1NXHQkf/nxQNZv380DHy/3HadZCbl5fsu3FJGVk8clo7uS3ibedxwR2Y9RPVI4L7MzU75YzeKNu3zHaTZCrtC37iono20ik8drAS6RpuyOU48guUU0t7+5gOoazU1vDCFX6GN7pfL+jeNokxDjO4qIHERyfAy/Pb0f89bv5Pmv1/iO0yyEXKEDmnMuEiLOGNyRo3ulcs/7y9hUuNt3nLAXkoUuIqHBzPjzmQOpdo7fTVvkO07YU6GLSIPqkhLPDcf15oPFW5i+cLPvOGFNhS4iDe6qo7vTt30Sv397EUVl2rKuoajQRaTBRUdGcPfZg9hSVMa97y/zHSdsqdBFpFEMSU/m0lFdee6btcxZt8N3nLCkQheRRnPzSX1olxTH7W8soLL6YKtxS12o0EWk0STFRfOHif1ZurmIKV+s9h0n7KjQRaRRndS/PSf2a8c/Pl7Oum2lvuOEFRW6iDS6P0zsT1REBL96S1vWBZMKXUQaXYdWLbj5xN58saKAaXM3+o4TNlToIuLFJaO7MTg9mT+9s5idpRW+44QFFbqIeBEZYdx91kB27q7kL+82+CZnzYIKXUS8OaJDS646ujuvZefx9cptvuOEPBW6iHh143G9SW/Tgl+9uYCySm1ZVx8qdBHxqkVMJHeeOZBVBSU8+ulK33FCmgpdRLz7Ue80Jg7pyGOf5pK7tch3nJClQheRJuE3E/oRHxPF7W8soEZb1tWJCl1EmoTUxFjuOLUv363ZwavZ633HCUkqdBFpMs7LTGdk9zbc9e4SPly8RVeRHiYVuog0GWbGX88eREpiLP/zXDZnP/YV36zSdMbaUqGLSJPSLTWBD34xjrvOGsiGnbu54IlvuPSpb1m4odB3tCbPfP2TJjMz02VnZ3t5bREJDWWV1Tz71Roe/XQlhbsrmTCoAzed2IfuqQm+o3ljZjnOucz9PqdCF5GmrnB3JU9+vop/zlxNRXUN52V25vrjetGhVQvf0RrdwQq91kMuZhZpZnPM7J39PBdrZq+aWa6ZzTKzbvXIKyLyH1q1iObmk/rw+a3juWRUV7Jy8jjmnk+5690l7CjRwl7fO5wx9BuAA62gcyWwwzmXAdwP/LW+wURE9pWWFMvvz+jPJzcdw2kDO/DEF6sY97cZPPTxCkrKq3zH865WhW5mnYHTgCkHOGQi8GzgdhZwnJlZ/eOJiPy39Dbx3Hf+EKbfMI5RPVP4+4fL+dE9M3jmy9WUVzXf9WBqe4b+AHArcKBdXTsB6wGcc1VAIZCy70FmNsnMss0sOz8///DTiojspU/7JJ68NJPXf34UPdMS+f2/FnPc3z/j9Zw8qpvh1aaHLHQzmwBsdc7l1PfFnHNPOOcynXOZaWlp9f1xIiIADO/amlcmjeLZK0bQqkU0N02dxyn/+JwPFm1uVhcn1eYMfQxwhpmtAV4BjjWzF/Y5ZgOQDmBmUUArQFcDiEijMTN+1DuNf107locvHEpltWPS8zmc9dhXzWat9UMWunPududcZ+dcN+AC4BPn3MX7HPY2cFng9jmBY5rPr0URaTIiIowJgzr+cHHSpp1l/OTJ5nFxUp2vFDWzP5rZGYG7/wRSzCwX+CVwWzDCiYjUVXRkBD8Z0YVPbzmGO07ty/y8nUx4aCaTX5rNqvxi3/EahC4sEpFmYVfZ/12cVF4Vuhcn6UpREZGA/KJyHpmRy4uz1mJmXH5UN37+o560TojxHa1WgnKlqIhIONj74qQJgzrw5BerOOUfX7B+e6nvaPWmQheRZim9TTz3nTeEaZPHsLuymoumzGLLrjLfsepFhS4izdqgzsk8e8UIthWXc/GUWWwP4bVhVOgi0uwNSU9mymVHsm57KZc99S27yip9R6oTFbqICDC6ZwqPXTyMJZt2ceUz37G7IvTWhFGhi4gEHNu3HQ9cMISctTuY9Hx2yC30pUIXEdnLhEEdufusQXyxooAbXp5LVfWB1iRselToIiL7OO/IdH4zoR/TF23m1tfnUxMiKzdG+Q4gItIUXTm2OyXlVdz34XISY6P4wxn9aerbPKjQRUQO4LpjMygur+KJz1eRFBfFLSf19R3poFToIiIHYGbcfkpfisqqeGTGShJio7jmmAzfsQ5IhS4ichBmxp1nDqC0ooq/TV9GUmwUl4zu5jvWfqnQRUQOITLCuPfcwZSUV/ObaYuIj4ni7OGdfcf6L5rlIiJSC9GRETx84VCO6pnCLVnzmL5wk+9I/0WFLiJSS3HRkTx5aSaD05O57uU5fLa8aW12r0IXETkMCbFRPHP5CDLaJvGz57P5bs1235F+oEIXETlMreKjef7KEXRMbsEVT3/HgrymsVepCl1EpA5SE2N54cqRtGwRzaVPzWLFliLfkVToIiJ11TG5BS9eNZKoyAgumjKLddv87nqkQhcRqYduqQm8cOVIKqpruHDKN2wu9LfrkQpdRKSe+rRP4tmfjmBnaSUXTfmGbcXlXnKo0EVEgmBwejJTLsskb8duLvW065EKXUQkSEb1SOHxS4azfEsRVzz9HaUVVY36+ip0EZEgGt+nLQ+cP5TZ63bws+dzGnXXIxW6iEiQnTaoA3efvWfXo+temtNoux6p0EVEGsB5men87vR+fLB4C7dkNc6uR1ptUUSkgfx0zJ5dj+79YDkJsZH8aeKABt316JCFbmZxwOdAbOD4LOfc7/Y55nLgHmBD4KGHnXNTghtVRCT0TB6fQVF5Ff/vs1UkxEZx28l9G6zUa3OGXg4c65wrNrNoYKaZveec+2af4151zl0b/IgiIqHLzLjt5L4Ul+0p9ZZx0Uwe3zC7Hh2y0J1zDigO3I0OfIXGFtgiIk2AmfGniQMoKa/inveXkRATyeVjugf9dWr1oaiZRZrZXGAr8KFzbtZ+DjvbzOabWZaZpR/g50wys2wzy87Pb1rrCIuINKSICOOecwdz+uCOdEmJb5DXsD0n4LU82CwZeBO4zjm3cK/HU4Bi51y5mf0MON85d+zBflZmZqbLzs6uW2oRkWbKzHKcc5n7e+6wpi0653YCM4CT93l8m3Pu+8ULpgDD65BTRETq4ZCFbmZpgTNzzKwFcAKwdJ9jOux19wxgSRAziohILdRmlksH4Fkzi2TPL4DXnHPvmNkfgWzn3NvA9WZ2BlAFbAcub6jAIiKyf4c1hh5MGkMXETl8QRtDFxGRpkuFLiISJlToIiJhQoUuIhImvH0oamb5wNo6/uepQEEQ4zQ14fz+9N5CVzi/v1B6b12dc2n7e8JbodeHmWUf6FPecBDO70/vLXSF8/sLl/emIRcRkTChQhcRCROhWuhP+A7QwML5/em9ha5wfn9h8d5CcgxdRET+W6ieoYuIyD5U6CIiYSLkCt3MTjazZWaWa2a3+c4TLGaWbmYzzGyxmS0ysxt8Zwq2wM5Xc8zsHd9Zgs3MkgO7dS01syVmNtp3pmAxs18E/k4uNLOXAxvHhywze8rMtprZ3pv0tDGzD81sReB7a58Z6yqkCj2whO8jwClAP+AnZtbPb6qgqQJucs71A0YBk8PovX3vBsJ3rfx/ANOdc32BwYTJ+zSzTsD1QKZzbgAQCVzgN1W9PcM+m/QAtwEfO+d6AR8H7oeckCp0YASQ65xb5ZyrAF4BJnrOFBTOuU3OudmB20XsKYROflMFj5l1Bk5jz45WYcXMWgHjgH8COOcqArt7hYsooIWZRQHxwEbPeerFOfc5e/Zt2NtE4NnA7WeBMxszU7CEWqF3AtbvdT+PMCq975lZN2AosL/NuEPVA8CtQI3nHA2hO5APPB0YUppiZgm+QwWDc24DcC+wDtgEFDrnPvCbqkG0c85tCtzeDLTzGaauQq3Qw56ZJQKvAzc653b5zhMMZjYB2Oqcy/GdpYFEAcOAx5xzQ4ESQvSf7PsKjCVPZM8vrY5Agpld7DdVw3J75nKH5HzuUCv0DUD6Xvc7Bx4LC2YWzZ4yf9E594bvPEE0BjjDzNawZ5jsWDN7wW+koMoD8pxz3/+LKos9BR8OjgdWO+fynXOVwBvAUZ4zNYQt3++NHPi+1XOeOgm1Qv8O6GVm3c0shj0fzrztOVNQmJmxZwx2iXPuPt95gsk5d7tzrrNzrht7/sw+cc6FzVmec24zsN7M+gQeOg5Y7DFSMK0DRplZfODv6HGEyQe++3gbuCxw+zJgmscsdVabTaKbDOdclZldC7zPnk/bn3LOLfIcK1jGAJcAC8xsbuCxO5xz7/qLJIfhOuDFwInGKuCnnvMEhXNulpllAbPZMxNrDiF+mbyZvQwcA6SaWR7wO+Bu4DUzu5I9y3qf5y9h3enSfxGRMBFqQy4iInIAKnQRkTChQhcRCRMqdBGRMKFCFxEJEyp0EZEwoUIXEQkT/x81wYwtxc24QAAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "temperatures = [4.4, 5.1, 6.1, 6.2, 6.1, 6.1, 5.7, 5.2, 4.7, 4.1, 3.9, 3.5]\n",
    "s7 = pd.Series(temperatures, name=\"Temperature\")\n",
    "s7.plot()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are *many* options for plotting your data. It is not necessary to list them all here: if you need a particular type of plot (histograms, pie charts, etc.), just look for it in the excellent [Visualization](http://pandas.pydata.org/pandas-docs/stable/visualization.html) section of pandas' documentation, and look at the example code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What next?\n",
    "As you probably noticed by now, pandas is quite a large library with *many* features. Although we went through the most important features, there is still a lot to discover. Probably the best way to learn more is to get your hands dirty with some real-life data. It is also a good idea to go through pandas' excellent [documentation](http://pandas.pydata.org/pandas-docs/stable/index.html), in particular the [Cookbook](http://pandas.pydata.org/pandas-docs/stable/cookbook.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional References\n",
    "\n",
    "* [Pandas cheatsheet](https://pandas.pydata.org/pandas-docs/stable/user_guide/10min.html)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "toc": {
   "toc_cell": false,
   "toc_number_sections": true,
   "toc_section_display": "none",
   "toc_threshold": 6,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
