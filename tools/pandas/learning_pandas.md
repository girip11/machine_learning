# Learning Pandas

## Tutorials

- [The Ultimate Guide to the Pandas Library for Data Science in Python](https://www.freecodecamp.org/news/the-ultimate-guide-to-the-pandas-library-for-data-science-in-python/)
- [Pandas tutorial Hands on ML](https://github.com/ageron/handson-ml2/blob/master/tools_pandas.ipynb)
- [Pandas Cookbook](https://pandas.pydata.org/pandas-docs/stable/user_guide/cookbook.html)
- [The Pandas DataFrame: Make Working With Data Delightful](https://realpython.com/pandas-dataframe/)

## Practice

- [Pynative Pandas Exercise](https://pynative.com/python-pandas-exercise/)
- [Machinelearningplus.com pandas exercise](https://www.machinelearningplus.com/python/101-pandas-exercises-python/)
