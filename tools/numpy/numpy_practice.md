# Learning to use numpy

* I use numpy documentation offline using zeal(On mac use Dash)
* Numpy tutorials
  * [Tutorial_1](./numpy_tutorial.ipynb)
  * [Tutorial_2](https://github.com/ageron/handson-ml2/blob/master/tools_numpy.ipynb)

* After going through these tutorials, practice some problems from these websites
  * [Numpy exercises](https://www.machinelearningplus.com/python/101-numpy-exercises-python/)
  * [Kaggle numpy practice](https://www.kaggle.com/python10pm/learn-numpy-the-hard-way-70-exercises-solutions)
  * [Pynative numpy exercises](https://pynative.com/python-numpy-exercise/)
