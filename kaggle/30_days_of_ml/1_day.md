# Kaggle Getting Started

- [kaggle.com/me](kaggle.com/me) takes you to your profile.

## Four categories

- Competitions
- Datasets
- Notebooks
- Discussion

In each category, you could grow from `Novice -> Contributor -> Expert -> Master -> Grandmaster`.

- [Progression](kaggle.com/progression) - How to progress to different levels.

## Beginner competitions

- Titanic competition.
