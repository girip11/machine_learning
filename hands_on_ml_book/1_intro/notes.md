# ML Landscape

## ML Challenges

Challenges due to the training data.

- Insufficient training data
- Nonrepresentative training data
  - Sampling bias
  - Non responsive bias in surveys
- Poor quality data with outliers, noise
- Irrelevant Features

Challenges due to the ML algorithms

- Overfitting
  We can avoid overfitting by

  - Selecting a simpler model
  - Reduce the number of attributes of the training data
  - Regularize the model(constraints)
  - Gather more data
  - Reduce the noise in the data

- Underfitting
  We can fix underfitting in the following ways
  - Select a more complex model
  - Better Feature engineering
  - Reduce the regularization(constraints) on the model.

In practice, we always make some assumptions about the data and only evaluate some models, because its not possible to evaluate a large number of models(ML algos).
