# End to End Machine learning

## ML Checklist

1. Frame the problem and look at the big picture.
2. Get the data.
3. Explore the data to gain insights.
4. Prepare the data to better expose the underlying data patterns to Machine Learning algorithms.
5. Explore many different models and shortlist the best ones.
6. Fine-tune your models and combine them into a great solution.
7. Present your solution.
8. Launch, monitor, and maintain your system

## Important points

- When you have multiple choices of transformations for a feature(ex: missing values with zero, median etc) treat the transformation like hyperparameter.
- Always prefer random search for hyperparameter tuning unless the parameters to explore are very limited. We could also use Bayesian optimization approaches.
