# README

This folder contains notes and example code tryouts from the book [Hands-on Machine Learning with Scikit-Learn and TensorFlow](https://learning.oreilly.com/library/view/hands-on-machine-learning/9781491962282/)

[Hands on ML Github repository](https://github.com/ageron/handson-ml2)
