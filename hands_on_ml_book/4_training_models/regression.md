# Regression models

## Gradient descent

* [Chain rule partial derivatives](https://www.youtube.com/watch?v=wl1myxrtQHQ&list=PLblh5JKOoLUICTaGLRoHQDuF_7q2GfuJF)
* [Batch gradient descent statquest](https://www.youtube.com/watch?v=sDv4f4s2SB8&list=PLblh5JKOoLUICTaGLRoHQDuF_7q2GfuJF)
* [Stochastic gradient descent statquest](https://www.youtube.com/watch?v=vMh0zPT0tLI&list=PLblh5JKOoLUICTaGLRoHQDuF_7q2GfuJF)

## Ridge regression

* [Ridge regression statquest](https://www.youtube.com/watch?v=Q81RR3yKn30)

## LASSO regression

* [LASSO regression statquest](https://www.youtube.com/watch?v=NGf0voTMlcs)

[Ridge vs LASSO visualization](https://www.youtube.com/watch?v=Xm2C_gTAl8c)

## Elastic Net Regression

* [Elasticnet Regression Statquest](https://www.youtube.com/watch?v=1dKRdX9bfIo)

---

## References

* [StatQuest youtube channel](https://www.youtube.com/channel/UCtYLUTtgS3k1Fg4y5tAhLbw)
