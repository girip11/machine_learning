# Data preprocessing

* Features are columns using which we can predict the value of the dependent variable.
* Features are also called as **independent variables**
* Variable to be predicted is called the **dependent variable**. This will usually be the last column of the dataset.
* Often the feature columns come before the dependent column.

## Handling missing data

* In a large dataset, we can filter out those records with missing fields. But when dataset is small or lots of records contain missing fields, then we cannot ignore those records from the dataset.

* Common strategy to fill missing values in case of numerical columns is to fill with **either mean or median of values present in other rows.**

## Encoding categorical data

* Categorical data is defined as variables with a finite set of label values.
* **One hot encoding** is one of the ways to encode categorical data.
* Categorical variables are called **nominal** when the values donot have any natural ordered relationship with each other.
* When categorical values have a natural ordering such categorical variables are referred to as **ordinal variables**.

**Why encoding?** - Many machine learning algorithms cannot operate on label data directly. They require all input variables and output variables to be numeric.

Categorical data can be converted into numerical data using

* Integer encoding
* One-Hot encoding

### Integer encoding

* Use this encoding **only when the categorical values have natural ordered relationship** with each other.
* Each categorical value is assigned integer value in order just like an **Enum construct** in programming languages.
* ML algorithms can harness this ordered numeric representation.

### One-Hot encoding

* Use this encoding when the categorial values are unrelated to each other.
* Using integer encoding for such values will result in poor performance/poor predictions.
* In one-hot encoding, each unique categorical value is represented using a binary variable that can have either 1 (presence of the value) or 0 (absence of the value.)

```text
red,green,blue
1,0,0 # red only
0,1,0 # green only
0,0,1 # blue only
```

## Training and test set

* Training set - training the ML model
* Test set - Evaluate model performance.

## Feature scaling

* Feature scaling is always applied after splitting the data into training set and test set.
* Put all features on the same scale. Avoid some features dominating other features.
* This is applied to some machine learning models.
* Feature scaling applied on X_train.

### Standardisation

* Feature values between -3 and 3
* Works well all the time.

```text
x subscript stand = (x - mean(x)) / stddev(x)
```

* mean and stddev are obtained from features present in the training data.
* Don't apply standardisation to columns whose values are already within -3 and 3. (i.e) don't apply to dummy variables(one hot encoded columns)
* Apply to numerical columns only.

### Normalization

* Feature values between 0 and 1
* Used when normal distribution of features.

```text
x subscript norm = (x - min(x)) / (max(x) - min(x))
```

---

## References

* [One Hot encoding](https://machinelearningmastery.com/why-one-hot-encode-data-in-machine-learning/)
