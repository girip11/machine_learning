# Linear Regression

* Regression models (both linear and non-linear) are used for predicting a real value, like salary for example.
* If your independent variable is time, then you are forecasting future values, otherwise your model is predicting present but unknown values.
* Regression technique vary from Linear Regression to SVR and Random Forests Regression.

* The dependent features are called the dependent variables, outputs, or responses.

* The independent features are called the independent variables, inputs, or predictors.

## Regression problem formulation

* When implementing linear regression of some dependent variable `𝑦` on the set of independent variables `𝐱 = (𝑥₁, …, 𝑥ᵣ)`, where `𝑟` is the number of predictors, you assume a linear relationship between 𝑦 and `𝐱: 𝑦 = 𝛽₀ + 𝛽₁𝑥₁ + ⋯ + 𝛽ᵣ𝑥ᵣ + 𝜀`. This equation is the **regression equation**. `𝛽₀, 𝛽₁, …, 𝛽ᵣ` are the regression coefficients, and `𝜀` is the random error.

* The estimated or predicted response, `𝑓(𝐱ᵢ)`, for each observation `𝑖 = 1, …, 𝑛`, should be as close as possible to the corresponding actual response `𝑦ᵢ`. The differences `𝑦ᵢ - 𝑓(𝐱ᵢ)` for all observations `𝑖 = 1, …, 𝑛,` are called the **residuals**. Regression is about determining the best predicted weights, that is the weights corresponding to the smallest residuals.

To get the best weights, you usually **minimize** the sum of squared residuals (SSR) for all observations `𝑖 = 1, …, 𝑛: SSR = Σᵢ(𝑦ᵢ - 𝑓(𝐱ᵢ))²`. This approach is called the **method of ordinary least squares**.

## Simple Linear Regression

`𝑓(𝑥) = 𝑏₀ + 𝑏₁𝑥`

## Multiple Linear Regression

`𝑓(𝑥₁, …, 𝑥ᵣ) = 𝑏₀ + 𝑏₁𝑥₁ + ⋯ +𝑏ᵣ𝑥ᵣ`

## Polynomial regression

`𝑓(𝑥) = 𝑏₀ + 𝑏₁𝑥 + 𝑏₂𝑥²`

Regression function `𝑓` can include non-linear terms such as `𝑏₂𝑥₁²`, `𝑏₃𝑥₁³`, or even `𝑏₄𝑥₁𝑥₂`, `𝑏₅𝑥₁²𝑥₂`, and so on.

In the case of two variables and the polynomial of degree 2, the regression function has this form: `𝑓(𝑥₁, 𝑥₂) = 𝑏₀ + 𝑏₁𝑥₁ + 𝑏₂𝑥₂ + 𝑏₃𝑥₁² + 𝑏₄𝑥₁𝑥₂ + 𝑏₅𝑥₂²`.

### Underfitting and overfitting

One very important question that might arise when you’re implementing polynomial regression is related to the **choice of the optimal degree of the polynomial regression function**.

> **Underfitting** occurs when a model can’t accurately capture the dependencies among data, usually as a consequence of its own simplicity. It often yields a low 𝑅² with known data and bad generalization capabilities when applied with new data.
>
> **Overfitting** happens when a model learns both dependencies among data and random fluctuations. In other words, a model learns the existing data too well. Complex models, which have many features or terms, are often prone to overfitting. When applied to known data, such models usually yield high 𝑅². However, they often don’t generalize well and have significantly lower 𝑅² when used with new data.

## Simple linear regression references

* [GFG: Simple linear regression](https://www.geeksforgeeks.org/python-linear-regression-using-sklearn/)

## Multiple linear regression references

* [Multiple Linear Regression using Python](https://www.geeksforgeeks.org/ml-multiple-linear-regression-using-python/)

* [Stepwise regression explained](https://datacadamia.com/data_mining/stepwise_regression)

---

## References

* [Linear Regression](https://realpython.com/linear-regression-in-python/)
