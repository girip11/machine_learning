# Simple Linear Regression

* Equation is similar to the slope intercept line form `y=mx+c`.
* m is the slope and c is the intercept

```text
<!-- slope b1 can be positive as well as negative -->
<!-- y = mx + c -->
<!-- m = (y2-y1)/(x2-x1) -->
y = b0 + b1 * x
```

* `y` is the dependent variable (variable that we are interested in predicting) and `x` is the independent variable (feature variable).

* Example `Salary = b0 + b1 * Experience`
* More the value of b1 (slope of the line),more will be the person's salary.

* In this model, we try to draw a line, which is closest to all the points that we have plotted from the training data.
* ![Simple Linear Regression](simple_linear_regression.png)

## Finding line of best fit

* This model is the trend line that best fits our data. ![Ordinary least squares](ordinay_least_squares.png)

* Line of best fit is computed using the **least squares method**.

* Best line of fit always passes through the mean coordinate.
* Once we know b1, we can calculate b0 by substituting the mean coordinate `(mean(x), mean(y))` for (x, y) in the slope intercept line equation form.

* Once we have b0 and b1 calculated, we have the line of best fit. This equation is used to predict future values.

* ![X, Y calculation](./mean_calculation.png)
* ![Slope calculation](./slope_calculation.png)

## Model evaluation

* By substituting x value present in training set in the calculated equation, we can get predicted value for y (call it y').
* We also have actual value of y from the training set.
* Distance between actual and expected `(y' - y)^2`
* ![Model evaluation](least_squares.png)

```text
<!-- standard error of the estimate is given by the formula-->
<!-- Root Mean Square Error>
sqrt( sum[ (y'-y)^2 ] /(n-2) )
<!-- n is the number of observations. When n is large n-2 becomes approximated to n -->
```

* The sum of squared errors is calculated for each regression line and conclude the best fit line having the least square value.

## Applications of linear regression(includes other regression techniques)

* Predict economic growth
* Predict GDP
* Predict product price
* House price
* Cricket score prediction player performance

---

## Additional references

* [Linear regression](https://www.youtube.com/watch?v=orQ-QGaOPIg)
* [Simplilearn linear regression](https://www.youtube.com/watch?v=NUXdtN1W1FE)
