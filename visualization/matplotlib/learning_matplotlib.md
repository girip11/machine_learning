# Using matplotlib

## Tutorials

* [Matplotlib tutorial](./matplotlib_tutorial.ipynb)
* [Matplotlib tutorial Hands on ML](https://github.com/ageron/handson-ml2/blob/master/tools_matplotlib.ipynb)
* [Visualize using matplotlib](https://www.machinelearningplus.com/category/plots/)

## Practice

* [Pynative Matplotlib Exercise](https://pynative.com/python-matplotlib-exercise/)
