# Deep learning book

* [Fastai discussion forum](https://forums.fast.ai/)
* [Website](https://course.fast.ai/)
* [Fastbook Github repo](https://github.com/fastai/fastbook)
* [Fastai video lectures](https://course.fast.ai/videos/)
