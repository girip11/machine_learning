# Machine Learning lectures from Andrew Ng

* [Youtube playlist](https://www.youtube.com/playlist?list=PLLssT5z_DsK-h9vYZkQkYNWcItqhlRJLN). Slides for this course can be found [here](http://www.holehouse.org/mlclass/). This course was taught on 2011.
  
* [Stanford Machine Learning course syllabus](http://cs229.stanford.edu/syllabus.html)
