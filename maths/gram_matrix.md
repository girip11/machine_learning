# Gram matrix

Given a set of vectors V with cardinality m, where each vector in V belongs to R<sup>n</sup>, Gram matrix G is given as matrix containing all possible inner products of vectors in V.

* Mathematically, V<sup>T</sup> * V results in the Gram matrix.

---

## References

* [Gram matrix](https://mathworld.wolfram.com/GramMatrix.html)
